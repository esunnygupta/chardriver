#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include "scull.h"

ssize_t charRead(struct file *pstFile, char __user *pUser, size_t len, loff_t *pOffset)
{
	dev_t dev;
	int count;
	struct charDriver *pstDriver;

	printk(KERN_INFO "%s init...\n", __func__);

	// Get pointer of driver structure from file structure.
	pstDriver = (struct charDriver *)pstFile->private_data;
	printk(KERN_INFO "pstDriver [%p]\n", pstDriver);

	// Get Major and Minor numbers.
	dev = pstDriver->dev;
	printk(KERN_INFO "major[%d] minor[%d]\n", MAJOR(dev), MINOR(dev));

	if(len <= 0)
	{
		printk(KERN_ERR "invalid length");
		return -1;
	}
	printk(KERN_INFO "len [%ld]\n", len);

	// Copy Kernel Buffer to User Buffer.
	if(NULL == pstDriver->data)
	{
		printk(KERN_ERR "Empty Kernel Buffer\n");
		return -1;
	}
	count = copy_to_user(pUser, pstDriver->data, len);
	printk(KERN_INFO "buffer [%s]\n", (char *)pstDriver->data);
	printk(KERN_INFO "bytes left to read [%d]\n", count);

	// Free Kernel Buffer.
	kfree(pstDriver->data);
	pstDriver->data = NULL;

	printk(KERN_INFO "%s exit...\n", __func__);
	return 0;
}
