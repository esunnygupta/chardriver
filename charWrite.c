#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include "scull.h"

ssize_t charWrite(struct file *pstFile, const char __user *pUser, size_t len, loff_t *pOffset)
{
	dev_t dev;
	int count;
	struct charDriver *pstDriver;

	printk(KERN_INFO "%s init...\n", __func__);

	// Get pointer of driver structure from file structure.
	pstDriver = (struct charDriver *)pstFile->private_data;
	printk(KERN_INFO "pstDriver [%p]\n", pstDriver);

	// Get Major and Minor numbers.
	dev = pstDriver->dev;
	printk(KERN_INFO "major[%d] minor[%d]\n", MAJOR(dev), MINOR(dev));

	if(len <= 0)
	{
		printk(KERN_INFO "invalid length...\n");
		return -1;
	}
	printk(KERN_INFO "len[%ld]\n", len);

	// Allocate Kernel Buffer.
	pstDriver->data = kmalloc(len, GFP_KERNEL);
	if(NULL == pstDriver->data)
	{
		printk(KERN_ERR "kmalloc() failed...\n");
		return -1;
	}

	// Copy User Buffer to Kernel Buffer.
	count = copy_from_user(pstDriver->data, pUser, len);
	printk(KERN_INFO "buffer [%s]\n", (char *)pstDriver->data);
	printk(KERN_INFO "bytes left to write [%d]\n", count);

	printk(KERN_INFO "%s exit...\n", __func__);
	return 0;
}
