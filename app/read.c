#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	int ret, fd, count=0;
	char rbuffer[100];

	fd = open("/dev/scull", O_RDWR);
	if(fd < 0)
	{
		printf("open() failed...\n");
		return -1;
	}
	count = read(fd, rbuffer, sizeof(rbuffer));
	if(count < 0)
	{
		printf("read() failed...\n");
		return -1;
	}
	printf("buffer read [%s]\n", rbuffer);
	close(fd);
	return 0;
}
