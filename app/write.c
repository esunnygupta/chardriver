#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	int ret, fd, count=0;
	char wbuffer[100], rbuffer[100];

	fd = open("/dev/scull", O_RDWR);
	if(fd < 0)
	{
		printf("open() failed...\n");
		return -1;
	}
	sprintf(wbuffer, "%s", "Hello World, I am Character Driver.");
	count = write(fd, wbuffer, sizeof(wbuffer));
	if(count < 0)
	{
		printf("write() failed...\n");
		return -1;
	}
	close(fd);
	return 0;
}
