INSTALLDIR=modules
ifneq (${KERNELRELEASE},)
	obj-m:= charDriver.o
	charDriver-objs:= init.o exit.o charOpen.o charRead.o charWrite.o charRelease.o
else
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)
defualt:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) $(INSTALLDIR)
	@mkdir $(INSTALLDIR)
	@mv *.ko *.o $(INSTALLDIR)
	@mv modules.order Module.symvers $(INSTALLDIR)
	@mv *.mod.c .*.o.cmd .*.ko.cmd $(INSTALLDIR)

clean:
	@rm -rf $(INSTALLDIR)
	@clear
endif
