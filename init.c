#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include "scull.h"
#include "fops.h"

struct charDriver stDriver;

static int initChar(void)
{
	int ret;

	printk(KERN_INFO "%s init...\n", __func__);
	
	// Registering a driver and a device in linux kernel.
	ret = alloc_chrdev_region(&stDriver.dev, 0, DEV_COUNT, DEVICE);
	if(ret < 0)
	{
		printk(KERN_ERR "alloc_chrdev_region() failed [%d]\n", ret);
		return -1;
	}

	// Registering driver as a character driver.
	cdev_init(&stDriver.stCdev, &fops);
	ret = cdev_add(&stDriver.stCdev, stDriver.dev, DEV_COUNT);
	if(ret < 0)
	{
		printk(KERN_ERR "cdev_add() failed...\n");
		goto exit;
	}

	// Creating a device class.
	stDriver.pstClass = class_create(THIS_MODULE, DEVICE);
	if(NULL == stDriver.pstClass)
	{
		printk(KERN_ERR "class_create() failed...\n");
		goto exit;
	}

	// Creating a device in devfs.
	stDriver.pstDev = device_create(stDriver.pstClass, NULL, stDriver.dev, NULL, DEVICE);
	if(NULL == stDriver.pstDev)
	{
		printk(KERN_ERR "device_create() failed...\n");
		goto exit;
	}

	printk(KERN_INFO "%s exit...\n", __func__);
	return 0;
exit:
	return -1;
}

module_init(initChar);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("esunnygupta");
