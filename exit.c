#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include "scull.h"

extern struct charDriver stDriver;

static void exitChar(void)
{
	printk(KERN_INFO"%s init...\n", __func__);

	// Removing Device from devfs.
	device_destroy(stDriver.pstClass, stDriver.dev);

	// Removing Device Class.
	class_destroy(stDriver.pstClass);

	// Removing Character Driver.
	cdev_del(&stDriver.stCdev);

	// Unregistering a driver and a device from linux kernel.
	unregister_chrdev_region(stDriver.dev, DEV_COUNT);

	printk(KERN_INFO"%s exit...\n", __func__);
}

module_exit(exitChar);
