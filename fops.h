#ifndef __FOPS_H__
#define __FOPS_H__

#include "scull.h"

struct file_operations fops=
{
	open:charOpen,
	read:charRead,
	write:charWrite,
	release:charRelease
};

#endif
