#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include "scull.h"

int charOpen (struct inode *pstInode, struct file *pstFile)
{
	struct charDriver *pstDriver;

	printk(KERN_INFO "%s init...\n", __func__);

	printk(KERN_INFO "major[%d] minor[%d]\n", MAJOR(pstInode->i_rdev), MINOR(pstInode->i_rdev));
	
	// Get pointer of driver structure containing cdev structure.
	pstDriver = container_of(pstInode->i_cdev, struct charDriver, stCdev);
	if(NULL == pstDriver)
	{
		printk(KERN_ERR "container_of() failed...\n");
		return -1;
	}

	// Store driver structure as a private data in inode structure.
	pstInode->i_private = pstDriver;
	printk(KERN_INFO "pstDriver [%p]\n", pstInode->i_private);

	// Store driver structure as a private data in file structure.
	pstFile->private_data = pstDriver;
	printk(KERN_INFO "pstDriver [%p]\n", pstFile->private_data);

	printk(KERN_INFO "%s exit...\n", __func__);
	return 0;
}
