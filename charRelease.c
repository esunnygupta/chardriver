#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>

int charRelease(struct inode *pstInode, struct file *pstFile)
{
	printk(KERN_INFO "%s init...\n", __func__);

	// Get Major and Minor numbers and private data of inode and file structure.
	printk(KERN_INFO "major[%d] minor[%d]\n", MAJOR(pstInode->i_rdev), MINOR(pstInode->i_rdev));
	printk(KERN_INFO "pstDriver [%p]\n", pstInode->i_private);
	printk(KERN_INFO "pstDriver [%p]\n", pstFile->private_data);

	// Free private data from inode and file structure.
	pstInode->i_private = NULL;
	pstFile->private_data = NULL;
	
	printk(KERN_INFO "%s exit...\n", __func__);
	return 0;
}
