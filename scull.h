#ifndef __SCULL_H__
#define __SCULL_H__

#define DEVICE "scull"
#define DEV_COUNT 1

struct charDriver
{
	dev_t dev;
	struct class *pstClass;
	struct device *pstDev;
	struct cdev stCdev;
	void *data;
};

int charOpen (struct inode *, struct file *);
ssize_t charRead(struct file *, char __user *, size_t, loff_t *);
ssize_t charWrite(struct file *, const char __user *, size_t, loff_t *);
int charRelease(struct inode *, struct file *);

#endif
